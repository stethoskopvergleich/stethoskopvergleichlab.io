---
layout: page
title: About
permalink: /about/
---

Muss es unbedingt Littmann sein? Sind Alternativen wirklich akustisch weniger gut? Das testen wir in diesem Blog. 
Wir sind Ärzte, Medizinstudenten und Notfallsanitäter und testen Stethoskope anderer Hersteller gegen den Platzhirschen Littmann. 
Als Referenzmodelle dienen das Littmann Classic III und das Littmann Cardiology III stellvertretend für zwei Preiskategorien. 
Nach und nach werden wir hier unsere Ergebnisse präsentieren. 

