---
layout: post
title:  "ERKA Stethoskope"
date:   2019-12-23 15:32:14 -0300
categories: jekyll update
---
wir haben am 23.12. gegen 12:00 Uhr eine Email an ERKA mit der Bitte um Testgeräte gesendet, also einen Tag vor Heiligabend. 
Um 18:00 Uhr hatten wir bereits eine Antwort mit Zusage. Das nennen wir mal einen guten Kunden-Support.
Wir sind sehr gespannt, welche Modelle uns zur Verfügung gestellt werden und wie sich diese dann schlagen werden. 
# ... to be continued ...